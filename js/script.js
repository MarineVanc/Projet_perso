$(document).ready(function(){
	//carousel des jeux
	if (typeof $.fn.owlCarousel === 'function') {
		$('.owl-carousel').owlCarousel({
			loop:true,
			margin: 10,
			center: true,
			dots: true,
			autoplay: true,
			autoplaySpeed: 4800,
			smartSpeed: 4800,
			responsive: {
				0 : {
					items : 1
				}
			}
		});
	}

	//header sticky
	var hauteur = 430;

	$(window).scroll(function () {
		if ($(this).scrollTop() > hauteur) {
			$('#header').removeClass('alt');

		} else {
			$('#header').addClass('alt');
		}
	});

	//animation des boutons sur le côté
	$('.icon li').on('mouseenter touchend', function(){
		$(this).addClass('active');
	});
	$('.icon li').on('mouseleave', function(){
		$(this).removeClass('active');
	});
});

//on place la rubrique presentation
    $('#presentation').css('margin-top', $(window).height() - 50)
